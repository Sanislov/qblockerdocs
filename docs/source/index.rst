Introduction
############

| I started to work on QBlocker five years ago when I missed the quick object creation of 3ds Max. As a hobby project, it was free to download for many years.

| Now I decided to revive the development and release a new version as a paid addon.
| But to be fair with my users I want to keep the original one as a free addon and support it to stay compatible with the future releases of Blender.


What is QBlocker
################

QBlocker is an object creation addon for Blender to create parametric objects with fast placement similar to 3ds Max. The tool includes some basic object types, custom snapping and a working plane for more precision.

Download
########

| QBlocker v0.21:
| https://sanislovart.gumroad.com/l/uubjq
| https://blendermarket.com/products/qblocker

| QBlocker free v0.1.61:
| https://sanislovart.gumroad.com/l/gOEV


Contact
#######

| If you need help or find a bug:
| sanislov@gmail.com

| Follow me on Twitter:
| https://twitter.com/SzeleczkiBalazs


Install
#######

* Edit -> Preferences -> Add-ons -> Install
* Select the downloaded ZIP file.
* Click the tickbox to enable the addon.
* Done!


.. toctree::
   :maxdepth: 1
   :caption: Documentation

   qblocker
   qblocker_free

